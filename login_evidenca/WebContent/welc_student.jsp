<!DOCTYPE html>
<html lang="sl">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/perfect-scrollbar/perfect-scrollbar.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->


	<script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(e) {
  if (!e.target.matches('.dropbtn')) {
  var myDropdown = document.getElementById("myDropdown");
    if (myDropdown.classList.contains('show')) {
      myDropdown.classList.remove('show');
    }
  }
}
</script>
<style>
.navbar {
  overflow: hidden;
  background-color: #333;
  font-family: Arial, Helvetica, sans-serif;
}

.navbar a {
  float: left;
  font-size: 16px;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

.dropdown {
  float: left;
  overflow: hidden;
}

.dropdown .dropbtn {
  cursor: pointer;
  font-size: 16px;  
  border: none;
  outline: none;
  color: white;
  padding: 14px 16px;
  background-color: inherit;
  font-family: inherit;
  margin: 0;
}

.navbar a:hover, .dropdown:hover .dropbtn, .dropbtn:focus {
  background-color:grey;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  float: none;
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {
  background-color: #ddd;
}

.show {
  display: block;
}
</style>
</head>
<body>
	
	<div class="limiter">
		<div class="container-table100">
			<div class="wrap-table100">
				<div class="table100">
				<div class="navbar">
  <a href="#home">Domov</a>
  <a href="#news">Moj profil</a>
  <div class="dropdown">
  <button class="dropbtn" onclick="myFunction()">Faze pisanja diplomske
    <i class="fa fa-caret-down"></i>
  </button>
  <div class="dropdown-content" id="myDropdown">
    <a href="#">Faze</a>
    <a href="#">Moja diplomska naloga</a>
    <a href="#">Moje opombe</a>
  </div>
  </div> 
</div>
<br>
<br>
					<table>
						<thead>
							<tr class="table100-head">
								<th class="column1">Faza</th>
								<th class="column2">Priporoceni rok</th>
								<th class="column3">Koncni rok</th>
								<th class="column7" >Opombe</th>
							</tr>
						</thead>
						<tbody>
								<tr>
									<td class="column1">Dispozicija</td>
									<td class="column2">12.2.2019</td>
									<td class="column3">25.2.2019</td>
									<td><input type="textfield" class="column4" ></td>
								</tr>
								<tr>
									<td class="column1">Oddaja dispozicije v referat</td>
									<td class="column2">20.03.2019</td>
									<td class="column3">01.04.2019</td>
									<td> <input type="textfield" class="column4"></td>
								</tr>
								<tr>
									<td class="column1">Osnutek diplome</td>
									<td class="column2">15.04.2019</td>
									<td class="column3">30.04.2019</td>
									<td><input type="textfield" class="column4"></td>
								</tr>
								<tr>
									<td class="column1">Oddaja diplome</td>
									<td class="column2">20.05.2019</td>
									<td class="column3">01.06.2019</td>
									<td><input type="textfield" class="column4"></td>
								</tr>
								<tr>
									<td class="column1">Zagovor diplome</td>
									<td class="column2">/</td>
									<td class="column3">30.06.2019</td>
									<td><input  type="textfield"class="column4"></td>
								</tr>
								
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>


	

<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>
	


</body>
</html>