<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/perfect-scrollbar/perfect-scrollbar.css">
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<title>Evidenca za diplomske naloge</title>
</head>
<body>

			<h1 align="center">Prijava</h1>
	  
	  	<br></br>
	    <div class="" align="center">
		    <form method="post" action="login.jsp">
	            <table align="center" cellspacing="10">
		                <tr>
		                	<td><b>Izberi uporabnika</b></td>
		                </tr>
		                <tr>
		            		<td><input type="checkbox" align="center" name="uporabnik" value="Student" /> Študent</td>
		            	</tr>
		            	<tr>
		             		<td><input type="checkbox" name="uporabnik" value="Profesor" /> Profesor</td>
		            	</tr>
	                    <tr>
	    	              
	                        <td><input type="submit" value="Prijava" /></td>
	                    </tr>
	            </table>
	        </form>
	  	</div>
   
<!--- banner wrapper div end -->
<div class="footer-wrapper">
  <div class="footer">
  </div>
</div>
	

<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>
</body>
</html>