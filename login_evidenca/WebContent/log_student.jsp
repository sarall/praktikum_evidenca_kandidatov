<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

</head>
<body>
	<br></br>

	<div class="menu-wrapper">
		<div class="menu">
			<ul>
				<li><a href="index.html" class="active">Domov</a></li>
			</ul>
		</div>
	</div>
	<!--- menu-wrapper div end -->
	<div class="clearing"></div>
	<div class="border-bottom"></div>
	<div class="clearing"></div>
	<div class="banner-wrapper">
		<div class="bannerlef">
			<img src="slike/banner-wrap-left.jpg" />
		</div>
		<div class="banner-container">
			<div class="banner">
				<br></br>
				<div class="">
					<h1 align="center">Prijava - Oseba</h1>
				</div>
				<br></br>
				<div class="" align="center">
					<form method="post" action="login_oseba.jsp">
						<table align="center" cellspacing="10">
							<tr>
								<td>Uporabniško ime</td>
								<td><input type="text" name="uporabnisko_ime" value="" /></td>
							</tr>
							<tr>
								<td>Geslo</td>
								<td><input type="password" name="geslo" value="" /></td>
							</tr>
							<tr>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td><input type="submit" value="Prijava" /></td>
							</tr>
						</table>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!--- banner wrapper div end -->
	<div class="footer-wrapper">
		<div class="footer">
			
		</div>
	</div>
</body>
</html>