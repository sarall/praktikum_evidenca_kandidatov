Evidenca kandidatov za diplomo

 

1. OPIS

Informacijska rešitev za študente v času pisanja diplome in njihove mentorje. Študent ima pregled nad fazo pisanja diplome, v kateri se trenutno nahaja, in nad roki, ki se približujejo. Profesor ima pregled nad študenti, ki jim je mentor, in tem, v kateri fazi se ti nahajajo. Ob približajočem se roku za zaključek katere od faz pisanja diplomske naloge študent prejme e-mail z obvestilom o tem. Študent in ptofesor med seboj lahko komunicirata s sporočili.


2. ARHITEKTURA

Klasična trodelna; shranjeni podatki -> poslovna logika -> uporabniški vmesnik


3. TEHNOLOGIJA
 
Izgled: html, css, bootstrap. Podatkovna baza: sql. Povezovanje: jsp


4. FUNKCIONALNOSTI

Vpis v sistem kot študent / profesor, pošiljanje sporočil med profesorjem in študentom, pošiljanje opomnikov, pregled faz, pregled študentov